﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface ISeedConfig
    {
        string Lang { get; set; }
        int RecordsCount { get; set; }
        bool Enable { get; set; } 
    }
}
