﻿using DAL.Entities; 

namespace DAL.Interfaces
{

    public interface IUnitOfWork
    {
        IRepository<Projects> Projects { get; }
        IRepository<Teams> Teams { get; }
        IRepository<Tasks> Tasks { get; }
        IRepository<Users> Users { get; }  
    }

}
