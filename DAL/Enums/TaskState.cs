﻿ 
namespace DAL.Enums
{ 
    public enum TaskSatate
    {
        Created = 0,
        InProgress = 1,
        Finished = 2,
        Cancelled = 3
    }
}
