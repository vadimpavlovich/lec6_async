﻿using Microsoft.EntityFrameworkCore;
using DAL.Entities;
using DAL.Services;
using DAL.Interfaces; 

namespace DAL
{
    public class Context : DbContext
    { 
        public Context(DbContextOptions<Context> options, ISeedConfig seedConfig = null) : base(options)
        {
            if (seedConfig == null)
                this.SeedService = new SeederService();
            else
                this.SeedService = new SeederService(seedConfig); 
        }
        public DbSet<Projects> Projects { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Teams> Teams { get; set; }
        private readonly SeederService SeedService;

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Projects>()
                .HasMany(p => p.Tasks)
                .WithOne(t => t.Project)
                .HasForeignKey(p => p.ProjectId);

            builder.Entity<Projects>()
                .HasOne(p => p.Author)
                .WithMany(u => u.Projects)
                .HasForeignKey(p => p.AuthorId);

            builder.Entity<Tasks>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId);

            builder.Entity<Teams>()
                .HasMany(p => p.Users)
                .WithOne(u => u.Team)
                .HasForeignKey(u => u.TeamId);

            builder.Entity<Projects>()
              .HasOne(p => p.Team)
              .WithMany(t => t.Projects)
              .HasForeignKey(p => p.TeamId);

            builder.Entity<Users>().Property(b => b.FirstName).HasMaxLength(40);   
            builder.Entity<Users>().Property(b => b.LastName).HasMaxLength(40);      
            builder.Entity<Users>().Property(b => b.LastName).HasMaxLength(40);
            builder.Entity<Users>().Property(b => b.Salary).HasPrecision(10, 2);


            if (SeedService.config.Enable)
            {
                var projects = SeedService.GetProjectsList();
                builder.Entity<Projects>().HasData(projects);
                var tasks = SeedService.GetTasksList();
                builder.Entity<Tasks>().HasData(tasks);
                var teams = SeedService.GetTeamsList();
                builder.Entity<Teams>().HasData(teams);
                var users = SeedService.GetUsersList();
                builder.Entity<Users>().HasData(users);
            }  
        }
    }
}
