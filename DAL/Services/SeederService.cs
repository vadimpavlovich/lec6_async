﻿using System;
using System.Collections.Generic;
using Bogus;
using Bogus.Extensions;
using DAL.Entities;
using DAL.Enums;
using DAL.Interfaces;

namespace DAL.Services
{
    public class SeederService
    {
        public ISeedConfig config { get; set; }
        public SeederService(ISeedConfig config)
        {
            this.config = config;
        }
        public SeederService()
        {
            this.config = new SeedConfig();
        }
        public List<Users> GetUsersList()
        {
            var Id = 1;

            var userFaker = new Faker<Users>(config.Lang)
                .CustomInstantiator(f => new Users())
                .RuleFor(u => u.Id, f => Id++)
                .RuleFor(u => u.TeamId, f => f.Random.Int(1, config.RecordsCount))
                .RuleFor(u => u.FirstName, f => f.Person.FirstName)
                .RuleFor(u => u.LastName, f => f.Person.LastName)
                .RuleFor(u => u.Email, f => f.Person.Email)
                .RuleFor(u => u.BirthDay, f => f.Date.Past(20, DateTime.Now.AddYears(-7)).Date)
                .RuleFor(u => u.RegisteredAt, (f, u) => f.Date.Between(u.BirthDay.AddYears(8), DateTime.Now));

            var users = userFaker.Generate(config.RecordsCount);
            return users;
        }

        public List<Teams> GetTeamsList()
        {
            var Id = 1;

            var teamsFaker = new Faker<Teams>(config.Lang)
                .CustomInstantiator(f => new Teams())
                .RuleFor(t => t.Id, f => Id++)
                .RuleFor(t => t.Name, f => f.Commerce.Department())
                .RuleFor(t => t.CreatedAt, f => f.Date.Past(4, DateTime.Now));

            var teams = teamsFaker.Generate(config.RecordsCount);
            return teams;
        }

        public List<Projects> GetProjectsList()
        {
            var Id = 1;

            var projectsFaker = new Faker<Projects>(config.Lang)
                .CustomInstantiator(f => new Projects())
                .RuleFor(p => p.Id, f => Id++)
                .RuleFor(p => p.AuthorId, f => f.Random.Int(1, config.RecordsCount))
                .RuleFor(p => p.TeamId, f => f.Random.Int(1, config.RecordsCount))
                .RuleFor(p => p.Name, f => f.Commerce.Product())
                .RuleFor(p => p.Description, f => f.Lorem.Sentences(f.Random.Int(1, 4)))
                .RuleFor(p => p.CreatedAt, f => f.Date.Past(4, DateTime.Now))
                .RuleFor(p => p.Deadline, (f, p) => f.Date.Between(p.CreatedAt, DateTime.Now.AddYears(1)));

            var projects = projectsFaker.Generate(config.RecordsCount);
            return projects;
        }

        public List<Tasks> GetTasksList()
        {
            var Id = 1;

            var tasksFaker = new Faker<Tasks>(config.Lang)
                .CustomInstantiator(f => new Tasks())
                .RuleFor(t => t.Id, f => Id++)
                .RuleFor(t => t.ProjectId, f => f.Random.Int(1, config.RecordsCount))
                .RuleFor(t => t.PerformerId, f => f.Random.Int(1, config.RecordsCount))
                .RuleFor(t => t.Name, f => f.Lorem.Sentence(f.Random.Int(3, 6)))
                .RuleFor(t => t.Description, f => f.Lorem.Sentences(f.Random.Int(1, 4)))
                .RuleFor(t => t.State, f => (TaskSatate)f.Random.Int(0, 3))
                .RuleFor(t => t.CreatedAt, f => f.Date.Past(4, DateTime.Now))
                .RuleFor(t => t.FinishedAt, (f, t) => f.Date.Between(t.CreatedAt, DateTime.Now).OrNull(f, .6f));

            var tasks = tasksFaker.Generate(config.RecordsCount);
            return tasks;
        }
    }
}