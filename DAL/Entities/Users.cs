﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class Users
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; } 
        public bool BlockAccess { get; set; }
        public decimal Salary { get; set; }

        public Teams Team { get; set; }
        public IEnumerable<Projects> Projects { get; set; }
        public IEnumerable<Tasks> Tasks { get; set; }
    }
}
