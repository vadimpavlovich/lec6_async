﻿using System;
using System.Collections.Generic; 

namespace DAL.Entities
{
    public class Teams
    { 
        public int Id { get; set; } 
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; }
        public int Efficiency { get; set; }

        public IEnumerable<Users> Users { get; set; }
        public IEnumerable<Projects> Projects { get; set; }
    }
}
