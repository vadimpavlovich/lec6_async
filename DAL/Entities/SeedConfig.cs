﻿using DAL.Interfaces; 

namespace DAL.Entities
{
    public class SeedConfig : ISeedConfig
    {
        public string Lang { get; set; } = "ru";
        public int RecordsCount { get; set; } = 20;
        public bool Enable { get; set; } = false;
    }
}