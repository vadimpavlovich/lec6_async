﻿using DAL.Entities;
using FluentValidation;
using System;

namespace DAL.Validators
{
    public class TasksValidator : AbstractValidator<Tasks>
    {
        public TasksValidator()
        {
            RuleFor(t => t.Name).NotEmpty().WithMessage("*Required");
            RuleFor(t => t.CreatedAt).NotEmpty().WithMessage("*Required")
                                     .LessThan(DateTime.Now)
                                     .WithMessage("CreatedAt cannot be in future");
            RuleFor(x => x.Description).NotEmpty().WithMessage("Description cannot be empty")
                                     .Length(5, 500).WithMessage("Description length is incorrect");
        }
    }
}