﻿using AutoMapper;
using DAL.ModelsDTO.WebAPI;
using DAL.Entities;

namespace DAL.MappingProfiles
{
    public sealed class ProjectsProfile : Profile
    {
        public ProjectsProfile()
        {
            CreateMap<Projects, ProjectDTO>(); 
            CreateMap<UpdateProjectDTO, Projects>(); 
            CreateMap<CreateProjectDTO, Projects>(); 
            CreateMap<Projects, CreateProjectDTO>(); 
        }
    }
}
