﻿using AutoMapper;
using DAL.ModelsDTO.WebAPI;
using DAL.Entities;

namespace DAL.MappingProfiles
{
    public sealed class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<Users, UserDTO>(); 
            CreateMap<UpdateUserDTO, Users>();
            CreateMap<CreateUserDTO, Users>();
        }
    }
}
