﻿using AutoMapper;
using DAL.ModelsDTO.WebAPI;
using DAL.Entities;

namespace DAL.MappingProfiles
{
    public sealed class TasksProfile : Profile
    {
        public TasksProfile()
        {
            CreateMap<Tasks, TaskDTO>();
            CreateMap<UpdateTaskDTO, Tasks>();
            CreateMap<CreateTaskDTO, Tasks>();
        }
    }
}
