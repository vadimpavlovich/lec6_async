﻿using AutoMapper;

namespace DAL.MappingProfiles
{
    public static class Mapper
    {
        public static IMapper GetMapper()
        {
            var MapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectsProfile>();
                cfg.AddProfile<TeamsProfile>();
                cfg.AddProfile<TasksProfile>();
                cfg.AddProfile<UsersProfile>();
            });
            return MapperConfig.CreateMapper();
        }
    }
}
