﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Efficiency = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BlockAccess = table.Column<bool>(type: "bit", nullable: false),
                    Salary = table.Column<decimal>(type: "decimal(10,2)", precision: 10, scale: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: true),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: true),
                    PerformerId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Efficiency", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2018, 6, 30, 15, 0, 38, 900, DateTimeKind.Local).AddTicks(5619), 0, "здоровье, украшения & промышленное" },
                    { 18, new DateTime(2019, 8, 23, 20, 12, 34, 127, DateTimeKind.Local).AddTicks(1457), 0, "Дом, садинструмент & туризм" },
                    { 17, new DateTime(2021, 6, 5, 2, 57, 33, 863, DateTimeKind.Local).AddTicks(5464), 0, "для малышей, красота & обувь" },
                    { 16, new DateTime(2020, 7, 5, 20, 43, 23, 769, DateTimeKind.Local).AddTicks(6526), 0, "Одежда" },
                    { 15, new DateTime(2020, 12, 2, 13, 32, 19, 639, DateTimeKind.Local).AddTicks(8006), 0, "Спорт" },
                    { 14, new DateTime(2018, 3, 29, 0, 42, 5, 730, DateTimeKind.Local).AddTicks(5566), 0, "компьютеры & Книги" },
                    { 13, new DateTime(2018, 6, 30, 10, 46, 8, 227, DateTimeKind.Local).AddTicks(1701), 0, "Бакалея" },
                    { 12, new DateTime(2020, 10, 5, 14, 47, 50, 37, DateTimeKind.Local).AddTicks(9549), 0, "украшения & туризм" },
                    { 11, new DateTime(2018, 12, 2, 18, 3, 8, 406, DateTimeKind.Local).AddTicks(5359), 0, "Спорт" },
                    { 10, new DateTime(2018, 8, 15, 0, 51, 5, 82, DateTimeKind.Local).AddTicks(7951), 0, "детское, Одежда & музыка" },
                    { 9, new DateTime(2019, 8, 10, 12, 21, 44, 449, DateTimeKind.Local).AddTicks(4722), 0, "Одежда, Игрушки & Автомобильное" },
                    { 8, new DateTime(2021, 4, 21, 17, 43, 39, 936, DateTimeKind.Local).AddTicks(7840), 0, "Спорт" },
                    { 7, new DateTime(2020, 6, 23, 17, 52, 14, 407, DateTimeKind.Local).AddTicks(8202), 0, "Игрушки, Фильмы & Одежда" },
                    { 6, new DateTime(2017, 10, 9, 7, 8, 39, 657, DateTimeKind.Local).AddTicks(9636), 0, "Бакалея" },
                    { 5, new DateTime(2020, 10, 16, 2, 22, 49, 738, DateTimeKind.Local).AddTicks(8256), 0, "Галантерея" },
                    { 4, new DateTime(2019, 11, 25, 3, 6, 16, 21, DateTimeKind.Local).AddTicks(3290), 0, "Бакалея" },
                    { 3, new DateTime(2020, 12, 31, 10, 56, 50, 216, DateTimeKind.Local).AddTicks(2949), 0, "Пряжа, Дом & Спорт" },
                    { 2, new DateTime(2020, 11, 27, 15, 44, 30, 157, DateTimeKind.Local).AddTicks(5978), 0, "Галантерея, игры & Галантерея" },
                    { 19, new DateTime(2018, 2, 4, 19, 4, 32, 830, DateTimeKind.Local).AddTicks(7966), 0, "для малышей, туризм & красота" },
                    { 20, new DateTime(2018, 6, 9, 6, 46, 34, 863, DateTimeKind.Local).AddTicks(9842), 0, "Дом & Игрушки" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "BlockAccess", "Email", "FirstName", "LastName", "RegisteredAt", "Salary", "TeamId" },
                values: new object[,]
                {
                    { 2, new DateTime(2004, 10, 17, 0, 0, 0, 0, DateTimeKind.Local), false, "Ekaterina_Dorofeeva5@yahoo.com", "Екатерина", "Дорофеева", new DateTime(2021, 2, 11, 0, 9, 25, 223, DateTimeKind.Local).AddTicks(4100), 0m, 1 },
                    { 12, new DateTime(2012, 3, 27, 0, 0, 0, 0, DateTimeKind.Local), false, "Lyudmila.Vlasova75@yandex.ru", "Людмила", "Власова", new DateTime(2021, 6, 20, 13, 13, 8, 275, DateTimeKind.Local).AddTicks(6539), 0m, 18 },
                    { 9, new DateTime(2001, 6, 2, 0, 0, 0, 0, DateTimeKind.Local), false, "Yaroslav.Petukhov91@mail.ru", "Ярослав", "Петухов", new DateTime(2017, 2, 14, 4, 54, 45, 550, DateTimeKind.Local).AddTicks(422), 0m, 16 },
                    { 20, new DateTime(2009, 6, 10, 0, 0, 0, 0, DateTimeKind.Local), false, "Vadim77@mail.ru", "Вадим", "Селезнев", new DateTime(2021, 2, 13, 16, 15, 43, 187, DateTimeKind.Local).AddTicks(8783), 0m, 15 },
                    { 13, new DateTime(2012, 5, 31, 0, 0, 0, 0, DateTimeKind.Local), false, "Fyodor0@yahoo.com", "Фёдор", "Николаев", new DateTime(2020, 10, 20, 23, 55, 47, 575, DateTimeKind.Local).AddTicks(6345), 0m, 15 },
                    { 4, new DateTime(2008, 12, 16, 0, 0, 0, 0, DateTimeKind.Local), false, "Veronika.Sidorova95@mail.ru", "Вероника", "Сидорова", new DateTime(2017, 11, 15, 23, 43, 23, 226, DateTimeKind.Local).AddTicks(9595), 0m, 12 },
                    { 8, new DateTime(2013, 5, 7, 0, 0, 0, 0, DateTimeKind.Local), false, "Aleksandra30@yahoo.com", "Александра", "Веселова", new DateTime(2021, 7, 1, 17, 54, 12, 446, DateTimeKind.Local).AddTicks(3350), 0m, 11 },
                    { 16, new DateTime(1996, 2, 12, 0, 0, 0, 0, DateTimeKind.Local), false, "Aleksandra_Likhacheva@yandex.ru", "Александра", "Лихачева", new DateTime(2017, 1, 25, 14, 32, 14, 512, DateTimeKind.Local).AddTicks(3747), 0m, 9 },
                    { 11, new DateTime(1995, 8, 13, 0, 0, 0, 0, DateTimeKind.Local), false, "Sergei87@yahoo.com", "Сергей", "Данилов", new DateTime(2008, 1, 8, 22, 35, 37, 868, DateTimeKind.Local).AddTicks(4816), 0m, 9 },
                    { 10, new DateTime(2004, 4, 15, 0, 0, 0, 0, DateTimeKind.Local), false, "Marina37@ya.ru", "Марина", "Архипова", new DateTime(2015, 1, 12, 12, 26, 58, 292, DateTimeKind.Local).AddTicks(5623), 0m, 9 },
                    { 6, new DateTime(2014, 4, 15, 0, 0, 0, 0, DateTimeKind.Local), false, "Albert_Panfilov14@gmail.com", "Альберт", "Панфилов", new DateTime(2021, 8, 30, 11, 6, 58, 566, DateTimeKind.Local).AddTicks(7221), 0m, 8 },
                    { 19, new DateTime(2005, 11, 9, 0, 0, 0, 0, DateTimeKind.Local), false, "Andrei_Kharitonov@mail.ru", "Андрей", "Харитонов", new DateTime(2015, 3, 8, 11, 48, 48, 390, DateTimeKind.Local).AddTicks(5024), 0m, 7 },
                    { 1, new DateTime(2008, 2, 15, 0, 0, 0, 0, DateTimeKind.Local), false, "Anatolii_Kolobov@gmail.com", "Анатолий", "Колобов", new DateTime(2017, 5, 2, 3, 11, 29, 855, DateTimeKind.Local).AddTicks(9917), 0m, 7 },
                    { 15, new DateTime(2004, 1, 26, 0, 0, 0, 0, DateTimeKind.Local), false, "Mikhail.Biryukov56@yahoo.com", "Михаил", "Бирюков", new DateTime(2019, 9, 15, 16, 20, 54, 742, DateTimeKind.Local).AddTicks(4844), 0m, 6 },
                    { 3, new DateTime(1999, 1, 19, 0, 0, 0, 0, DateTimeKind.Local), false, "Olga_Lobanova23@hotmail.com", "Ольга", "Лобанова", new DateTime(2007, 1, 27, 8, 41, 17, 362, DateTimeKind.Local).AddTicks(4219), 0m, 6 },
                    { 14, new DateTime(2003, 6, 7, 0, 0, 0, 0, DateTimeKind.Local), false, "Georgii_Krylov@ya.ru", "Георгий", "Крылов", new DateTime(2015, 4, 14, 15, 54, 18, 451, DateTimeKind.Local).AddTicks(1876), 0m, 4 },
                    { 18, new DateTime(2005, 4, 7, 0, 0, 0, 0, DateTimeKind.Local), false, "Maksim_Kazakov@hotmail.com", "Максим", "Казаков", new DateTime(2017, 2, 15, 19, 43, 46, 352, DateTimeKind.Local).AddTicks(1101), 0m, 2 },
                    { 17, new DateTime(2009, 11, 15, 0, 0, 0, 0, DateTimeKind.Local), false, "Anton59@ya.ru", "Антон", "Агафонов", new DateTime(2019, 10, 17, 21, 50, 43, 987, DateTimeKind.Local).AddTicks(8283), 0m, 1 },
                    { 5, new DateTime(2003, 9, 27, 0, 0, 0, 0, DateTimeKind.Local), false, "Galina61@gmail.com", "Галина", "Галкина", new DateTime(2016, 12, 2, 8, 40, 37, 492, DateTimeKind.Local).AddTicks(1938), 0m, 20 },
                    { 7, new DateTime(2003, 6, 15, 0, 0, 0, 0, DateTimeKind.Local), false, "Kira.Sysoeva@hotmail.com", "Кира", "Сысоева", new DateTime(2020, 3, 23, 20, 37, 26, 218, DateTimeKind.Local).AddTicks(7128), 0m, 20 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 7, 2, new DateTime(2021, 1, 26, 2, 59, 23, 722, DateTimeKind.Local).AddTicks(205), new DateTime(2022, 4, 16, 19, 16, 6, 566, DateTimeKind.Local).AddTicks(9166), "Постоянный отметить прогресса национальный путь значительной от.\nНамеченных воздействия новых финансовых организации на повседневная.\nДальнейшее дальнейшее последовательного обуславливает.\nПредставляет роль не.", "Носки", 3 },
                    { 3, 5, new DateTime(2019, 4, 14, 0, 35, 43, 60, DateTimeKind.Local).AddTicks(7576), new DateTime(2022, 6, 24, 17, 3, 51, 769, DateTimeKind.Local).AddTicks(5029), "На занимаемых обеспечение массового показывает.\nСтруктуры разработке высокотехнологичная курс.", "Автомобиль", 8 },
                    { 1, 5, new DateTime(2020, 9, 22, 14, 51, 38, 72, DateTimeKind.Local).AddTicks(2416), new DateTime(2020, 12, 11, 5, 43, 33, 866, DateTimeKind.Local).AddTicks(201), "Предложений дальнейшее современного нашей вызывает.\nИдейные напрямую на.", "Куртка", 17 },
                    { 10, 12, new DateTime(2020, 4, 30, 21, 33, 29, 590, DateTimeKind.Local).AddTicks(5699), new DateTime(2021, 9, 18, 3, 12, 9, 812, DateTimeKind.Local).AddTicks(8260), "Определения внедрения требует.", "Майка", 19 },
                    { 15, 9, new DateTime(2017, 7, 16, 13, 10, 14, 393, DateTimeKind.Local).AddTicks(1039), new DateTime(2017, 8, 26, 21, 27, 57, 356, DateTimeKind.Local).AddTicks(269), "Условий поэтапного формирования внедрения прежде.\nПринципов от ресурсосберегающих прогресса реализация широким новая.\nИнформационно-пропогандистское принимаемых повседневная социально-ориентированный.", "Берет", 6 },
                    { 12, 20, new DateTime(2018, 11, 29, 7, 41, 26, 820, DateTimeKind.Local).AddTicks(1545), new DateTime(2021, 12, 5, 21, 3, 12, 867, DateTimeKind.Local).AddTicks(7874), "Определения принимаемых активности.\nПринципов современного обеспечивает качества значимость технологий.", "Компьютер", 18 },
                    { 14, 13, new DateTime(2018, 2, 8, 2, 14, 3, 680, DateTimeKind.Local).AddTicks(7275), new DateTime(2021, 8, 30, 11, 27, 39, 553, DateTimeKind.Local).AddTicks(1775), "Эксперимент с начало намеченных соображения задач форм забывать.\nПравительством задача значительной развития.", "Портмоне", 10 },
                    { 16, 16, new DateTime(2019, 8, 5, 15, 35, 6, 561, DateTimeKind.Local).AddTicks(6687), new DateTime(2020, 2, 3, 2, 44, 21, 209, DateTimeKind.Local).AddTicks(9167), "Занимаемых зависит другой отношении концепция мира.\nИдейные технологий систему новая создание.\nРазработке инновационный подготовке организации общественной.\nТакже значительной соответствующей повседневной предложений задач требует.", "Свитер", 7 },
                    { 9, 11, new DateTime(2017, 11, 20, 23, 40, 30, 314, DateTimeKind.Local).AddTicks(7476), new DateTime(2019, 4, 10, 7, 51, 45, 500, DateTimeKind.Local).AddTicks(4709), "Системы сфера обществом не систему играет.\nА условий понимание от что систему качественно качественно степени ресурсосберегающих.\nНовая модели принципов работы активизации.", "Ботинок", 20 },
                    { 5, 11, new DateTime(2021, 1, 26, 19, 35, 47, 835, DateTimeKind.Local).AddTicks(1453), new DateTime(2021, 3, 23, 17, 17, 44, 226, DateTimeKind.Local).AddTicks(4687), "Проект идейные по занимаемых этих сомнений.\nВысокотехнологичная консультация роль значимость курс обуславливает новая шагов занимаемых.\nПринципов потребностям специалистов повышение формированию формированию.", "Сабо", 4 },
                    { 2, 11, new DateTime(2018, 3, 1, 17, 41, 29, 908, DateTimeKind.Local).AddTicks(2592), new DateTime(2018, 9, 5, 6, 37, 55, 458, DateTimeKind.Local).AddTicks(4529), "Кадровой гражданского общественной.\nДля отношении стороны поэтапного кадров путь.\nРеализация значимость постоянное порядка развития.", "Сабо", 7 },
                    { 8, 10, new DateTime(2019, 1, 26, 10, 56, 2, 647, DateTimeKind.Local).AddTicks(7481), new DateTime(2019, 5, 6, 15, 25, 59, 556, DateTimeKind.Local).AddTicks(1065), "Разработке уточнения показывает актуальность формированию концепция.", "Берет", 5 },
                    { 20, 6, new DateTime(2019, 4, 22, 14, 17, 53, 301, DateTimeKind.Local).AddTicks(9970), new DateTime(2021, 8, 24, 0, 41, 42, 352, DateTimeKind.Local).AddTicks(4235), "Гражданского постоянный степени формировании общественной структуры уточнения процесс.", "Берет", 5 },
                    { 19, 6, new DateTime(2021, 4, 9, 3, 35, 25, 571, DateTimeKind.Local).AddTicks(4863), new DateTime(2021, 12, 3, 3, 45, 26, 51, DateTimeKind.Local).AddTicks(6822), "Национальный обеспечение задача кадров профессионального обществом базы повседневной.\nСложившаяся целесообразности профессионального.", "Шарф", 2 },
                    { 4, 15, new DateTime(2019, 11, 12, 5, 36, 35, 289, DateTimeKind.Local).AddTicks(296), new DateTime(2021, 11, 3, 2, 19, 1, 120, DateTimeKind.Local).AddTicks(31), "Консультация задача для.", "Кошелек", 4 },
                    { 17, 14, new DateTime(2019, 10, 29, 1, 48, 55, 161, DateTimeKind.Local).AddTicks(8186), new DateTime(2022, 2, 8, 5, 20, 59, 51, DateTimeKind.Local).AddTicks(5604), "Равным обществом обеспечение.\nЭкономической однако направлений образом следует по.", "Кепка", 12 },
                    { 6, 18, new DateTime(2020, 6, 15, 0, 23, 25, 135, DateTimeKind.Local).AddTicks(9427), new DateTime(2021, 4, 10, 22, 55, 24, 104, DateTimeKind.Local).AddTicks(8731), "Кадровой позволяет уточнения с занимаемых оценить поэтапного выполнять.\nЗначение укрепления влечёт предпосылки общества участниками экономической.\nСистему обществом понимание влечёт повседневная за соответствующей управление рамки.", "Кошелек", 7 },
                    { 18, 17, new DateTime(2019, 8, 31, 0, 11, 45, 797, DateTimeKind.Local).AddTicks(6345), new DateTime(2020, 12, 7, 18, 0, 40, 714, DateTimeKind.Local).AddTicks(3330), "На управление специалистов активности задания нашей.\nПодготовке модели нашей социально-экономическое для позиции модели задач.\nРесурсосберегающих представляет количественный социально-экономическое.\nОт экономической за.", "Компьютер", 5 },
                    { 11, 5, new DateTime(2020, 4, 23, 9, 57, 52, 73, DateTimeKind.Local).AddTicks(4786), new DateTime(2021, 4, 23, 22, 4, 10, 372, DateTimeKind.Local).AddTicks(2200), "Важные общественной соображения настолько внедрения влечёт значимость активизации напрямую информационно-пропогандистское.\nРамки последовательного обеспечивает соображения внедрения структуры повышению выбранный задач.", "Автомобиль", 8 },
                    { 13, 5, new DateTime(2020, 5, 1, 18, 32, 43, 424, DateTimeKind.Local).AddTicks(7678), new DateTime(2021, 10, 29, 14, 12, 28, 615, DateTimeKind.Local).AddTicks(1514), "Значительной рост очевидна от очевидна порядка не сущности а.", "Кошелек", 8 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 5, new DateTime(2017, 10, 3, 14, 47, 33, 58, DateTimeKind.Local).AddTicks(5648), "Значимость сомнений новых обеспечение забывать повышение.\nУправление постоянное общественной повседневной.", null, "Всего административных информационно-пропогандистское.", 20, 7, 2 },
                    { 3, new DateTime(2019, 12, 9, 11, 14, 51, 358, DateTimeKind.Local).AddTicks(789), "Концепция предпосылки путь системы уточнения значительной мира интересный стороны укрепления.\nПорядка порядка прогрессивного.\nШироким гражданского активности сомнений предпосылки условий.", new DateTime(2020, 6, 25, 16, 8, 42, 252, DateTimeKind.Local).AddTicks(2332), "Широкому развития реализация сложившаяся.", 8, 1, 2 },
                    { 4, new DateTime(2017, 7, 13, 9, 2, 23, 694, DateTimeKind.Local).AddTicks(9905), "Качества изменений однако широкому.\nИдейные задач не.\nОпределения участия общества.\nРазнообразный высокотехнологичная административных постоянное разнообразный что.", null, "Эксперимент повседневной организационной участия.", 3, 15, 3 },
                    { 16, new DateTime(2017, 11, 28, 10, 24, 11, 61, DateTimeKind.Local).AddTicks(1615), "Анализа задач широким сознания обучения намеченных а.\nКадровой вызывает формированию современного.\nПредставляет процесс общественной массового.", new DateTime(2020, 9, 13, 2, 45, 15, 473, DateTimeKind.Local).AddTicks(9995), "Же что формирования изменений социально-экономическое.", 7, 12, 1 },
                    { 17, new DateTime(2021, 4, 29, 9, 26, 32, 791, DateTimeKind.Local).AddTicks(7093), "Специалистов а же значительной определения отметить.\nПозиции потребностям активом роль.\nСоциально-ориентированный важную курс.\nДеятельности влечёт особенности повышение проект следует участия уровня условий.", new DateTime(2021, 6, 10, 23, 12, 40, 670, DateTimeKind.Local).AddTicks(2304), "Другой показывает нами за.", 3, 16, 1 },
                    { 2, new DateTime(2020, 2, 13, 7, 8, 8, 719, DateTimeKind.Local).AddTicks(8044), "Стороны демократической рост анализа обуславливает отметить прежде.", new DateTime(2020, 3, 7, 16, 52, 54, 596, DateTimeKind.Local).AddTicks(1440), "Занимаемых работы настолько.", 20, 16, 3 },
                    { 19, new DateTime(2019, 3, 8, 17, 6, 54, 860, DateTimeKind.Local).AddTicks(8665), "Информационно-пропогандистское массового задач.", new DateTime(2021, 4, 19, 21, 41, 43, 624, DateTimeKind.Local).AddTicks(8476), "Консультация работы обуславливает следует.", 7, 2, 1 },
                    { 1, new DateTime(2018, 7, 4, 23, 55, 58, 687, DateTimeKind.Local).AddTicks(5152), "Равным выполнять и важные выбранный соответствующей участия принимаемых.\nИнтересный высокотехнологичная структуры в.\nВыполнять подготовке опыт систему.", null, "Широкому различных однако рост насущным влечёт.", 11, 2, 1 },
                    { 8, new DateTime(2017, 8, 6, 11, 52, 44, 188, DateTimeKind.Local).AddTicks(13), "Позиции отношении общества и социально-ориентированный шагов целесообразности участия отношении.\nПозиции и выбранный намеченных профессионального организационной.\nЗначимость дальнейших прежде целесообразности потребностям этих.", null, "Следует позволяет порядка следует также различных.", 5, 8, 1 },
                    { 7, new DateTime(2020, 6, 4, 18, 36, 35, 782, DateTimeKind.Local).AddTicks(4040), "Поставленных инновационный равным насущным реализация значимость модернизации этих от напрямую.\nНаправлений насущным отметить предложений степени однако создание воздействия место способствует.", null, "Развития различных задач дальнейшее кадровой очевидна.", 17, 8, 3 },
                    { 14, new DateTime(2019, 2, 6, 3, 6, 24, 944, DateTimeKind.Local).AddTicks(780), "Реализация богатый место развития массового.\nЗначительной идейные сознания.\nПонимание потребностям различных выбранный значение не.\nОпределения правительством подготовке кругу насущным мира воздействия принимаемых другой.", null, "Нами гражданского системы.", 1, 19, 3 },
                    { 13, new DateTime(2019, 8, 22, 2, 30, 23, 327, DateTimeKind.Local).AddTicks(876), "Выполнять особенности внедрения систему показывает начало актуальность кадров отметить насущным.\nВысокотехнологичная позиции правительством укрепления.\nПрежде базы однако определения направлений по современного экономической.\nЗабывать повседневной поставленных проверки формированию всего социально-экономическое.", new DateTime(2021, 6, 20, 13, 12, 14, 225, DateTimeKind.Local).AddTicks(8078), "Материально-технической значение реализация специалистов качественно интересный.", 3, 19, 3 },
                    { 9, new DateTime(2021, 1, 25, 3, 20, 12, 437, DateTimeKind.Local).AddTicks(1721), "Формирования напрямую сущности отношении сущности.\nНапрямую предложений уровня социально-экономическое активизации также обуславливает также выполнять намеченных.\nПостоянное богатый специалистов целесообразности отношении воздействия инновационный определения.\nАктуальность деятельности настолько эксперимент таким не качества богатый.", new DateTime(2021, 4, 28, 0, 21, 0, 849, DateTimeKind.Local).AddTicks(6721), "Особенности проект социально-ориентированный.", 16, 19, 2 },
                    { 10, new DateTime(2017, 8, 1, 9, 54, 35, 84, DateTimeKind.Local).AddTicks(2706), "Работы сознания сознания.\nСовременного этих на.\nКонцепция принципов анализа деятельности формирования играет.\nУточнения форм представляет предпосылки начало.", new DateTime(2020, 8, 21, 13, 10, 31, 157, DateTimeKind.Local).AddTicks(6852), "Широким выбранный подготовке шагов отметить.", 13, 6, 0 },
                    { 6, new DateTime(2017, 8, 24, 6, 29, 44, 921, DateTimeKind.Local).AddTicks(8969), "Забывать позиции а гражданского экономической участниками для зависит другой.\nАктуальность ресурсосберегающих принимаемых воздействия место формировании отметить однако.\nНе позиции особенности задач кругу условий проект финансовых прогрессивного.\nКачественно национальный проект информационно-пропогандистское активности интересный структуры за обеспечивает.", null, "Широкому формировании сложившаяся.", 9, 6, 0 },
                    { 18, new DateTime(2019, 4, 19, 20, 51, 4, 638, DateTimeKind.Local).AddTicks(7271), "Качества с также таким.", null, "Играет социально-экономическое количественный формированию прежде шагов.", 20, 18, 0 },
                    { 15, new DateTime(2021, 4, 5, 6, 58, 34, 730, DateTimeKind.Local).AddTicks(4630), "С существующий актуальность место условий нашей в.\nСпециалистов рост отметить проект за нами активизации нас рост.", null, "Особенности качественно по структуры социально-экономическое.", 12, 7, 0 },
                    { 11, new DateTime(2019, 10, 7, 6, 25, 54, 913, DateTimeKind.Local).AddTicks(3720), "Условий шагов однако последовательного значимость сомнений предпосылки качественно разработке кадров.\nУчастия процесс дальнейших работы новая реализация зависит поэтапного напрямую.", new DateTime(2021, 5, 5, 15, 14, 11, 664, DateTimeKind.Local).AddTicks(495), "Экономической обеспечение позволяет.", 9, 7, 3 },
                    { 12, new DateTime(2021, 7, 6, 12, 4, 56, 412, DateTimeKind.Local).AddTicks(1506), "Экономической сложившаяся работы реализация.", new DateTime(2021, 7, 7, 17, 53, 5, 42, DateTimeKind.Local).AddTicks(8167), "Насущным современного направлений важные рост начало.", 17, 1, 0 },
                    { 20, new DateTime(2018, 5, 1, 15, 9, 15, 217, DateTimeKind.Local).AddTicks(1792), "Участия технологий базы сфера напрямую.\nШироким плановых модель определения реализация.", null, "Эксперимент сознания нами значение забывать укрепления.", 18, 1, 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
