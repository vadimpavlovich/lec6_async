﻿using DAL.Entities;

namespace DAL.ModelsDTO.ConsoleClient
{
    public class UserStatisticsDTO
    {
        /// <summary>
        /// Користувач
        /// </summary>
        public Users User { get; set; }
        /// <summary>
        /// Останній проект користувача (за датою створення)
        /// </summary>
        public Projects LastProject { get; set; }
        /// <summary>
        /// Загальна кількість тасків під останнім проектом
        /// </summary>
        public int TasksInLastProject { get; set; }
        /// <summary>
        /// Загальна кількість незавершених або скасованих тасків для користувача
        /// </summary>
        public int CanceledTasksCount { get; set; }
        /// <summary>
        /// Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
        /// </summary>
        public Tasks LongestTask { get; set; }
    }
}
