﻿using DAL.Enums;
using System;

namespace DAL.ModelsDTO.WebAPI
{
    public class CreateTaskDTO
    { 
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskSatate State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; } 
    }
}
