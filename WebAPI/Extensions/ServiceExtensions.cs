﻿using AutoMapper;
using DAL.MappingProfiles; 
using Microsoft.Extensions.DependencyInjection; 
using System.Reflection; 

namespace WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectsProfile>();
                cfg.AddProfile<TeamsProfile>();
                cfg.AddProfile<TasksProfile>();
                cfg.AddProfile<UsersProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
