﻿using AutoMapper;
using DAL.Entities;
using DAL.ModelsDTO.WebAPI;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Microsoft.EntityFrameworkCore;
using DAL.Validators;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly Context db;
        private readonly IMapper mapper;
        public UsersController(Context db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserDTO>>> GetUsersList()
        {
            List<Users> Users = await db.Users.ToListAsync();
            return Ok(mapper.Map<List<UserDTO>>(Users));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUserByID(int id)
        {
            if (!await db.Users.Where(x => x.Id == id).AnyAsync())
                return NotFound();
            Users User = db.Users.Where(x => x.Id == id).FirstOrDefault();
            return Ok(mapper.Map<UserDTO>(User));
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> CreateUser([FromBody] CreateUserDTO User)
        {
            var validator = new UsersValidator();
            Users newUser = mapper.Map<Users>(User);
            if (!validator.Validate(newUser).IsValid)
                return BadRequest(validator.Validate(newUser).Errors);
            db.Users.Add(newUser);
            await db.SaveChangesAsync();
            return Created("", mapper.Map<UserDTO>(newUser));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<UserDTO>> UpdateUser(int id, [FromBody] UpdateUserDTO User)
        {
            var validator = new UsersValidator();
            Users newUser = mapper.Map<Users>(User);
            if (!validator.Validate(newUser).IsValid)
                return BadRequest(validator.Validate(newUser).Errors);
            var updated = mapper.Map<Users>(User); 
            var exist = await db.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (exist != null)
            {
                updated.Id = exist.Id;
                db.Users.Remove(exist);
                db.Users.Add(updated);
                await db.SaveChangesAsync();
            } 
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<UserDTO>> DeleteUser(int id)
        {
            var User = await db.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (User != null)
            {
                db.Users.Remove(User);
                await db.SaveChangesAsync();
            }
            return NoContent();
        }
    }
}
