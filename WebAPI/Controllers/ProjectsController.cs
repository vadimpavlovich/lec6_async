﻿using AutoMapper;
using DAL.Entities;
using DAL.ModelsDTO.WebAPI;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Microsoft.EntityFrameworkCore;
using DAL.Validators;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly Context db;
        private readonly IMapper mapper;
        public ProjectsController(Context db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProjectDTO>>> GetProjectsList()
        {
            List<Projects> projects = await db.Projects.ToListAsync();
            return Ok(mapper.Map<List<ProjectDTO>>(projects));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectByID(int id)
        {
            if (!await db.Projects.Where(x => x.Id == id).AnyAsync())
                return NotFound();
            Projects project = db.Projects.Where(x => x.Id == id).FirstOrDefault();
            return Ok(mapper.Map<ProjectDTO>(project));
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> CreateProject([FromBody] CreateProjectDTO project)
        {
            try
            {
                var validator = new ProjectsValidator();
                Projects newProject = mapper.Map<Projects>(project);
                if (!validator.Validate(newProject).IsValid)
                    return BadRequest(validator.Validate(newProject).Errors);
                db.Projects.Add(newProject);
                await db.SaveChangesAsync();
                return Created("", mapper.Map<ProjectDTO>(newProject));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ProjectDTO>> UpdateProject(int id, [FromBody] UpdateProjectDTO project)
        {
            var validator = new ProjectsValidator();
            Projects newProject = mapper.Map<Projects>(project);
            if (!validator.Validate(newProject).IsValid)
                return BadRequest(validator.Validate(newProject).Errors);
            var updated = mapper.Map<Projects>(project); 
            var exist = await db.Projects.FirstOrDefaultAsync(x => x.Id == id);
            if (exist != null)
            {
                updated.Id = exist.Id;
                db.Projects.Remove(exist);
                db.Projects.Add(updated);
                await db.SaveChangesAsync();
            } 
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ProjectDTO>> DeleteProject(int id)
        {
            var Project = await db.Projects.FirstOrDefaultAsync(x => x.Id == id);
            if (Project != null)
            {
                db.Projects.Remove(Project);
                await db.SaveChangesAsync();
            }
            return NoContent();
        }
    }
}
