﻿using DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Services
{
    public class RequestService
    {
        private string ApiUrl { get; set; }
        private HttpClient client { get; set; }
        public List<Projects> projects { get; set; }
        public List<Teams> teams { get; set; }
        public List<Users> users { get; set; }
        public List<Tasks> tasks { get; set; }
        public RequestService(string ApiUrl)
        {
            this.ApiUrl = ApiUrl;
            client = new HttpClient();
        } 

        public async Task LoadDataAsync()
        {
            var projectsTask = GetProjectsAsync();
            var tasksTask = GetTasksAsync();
            var teamsTask = GetTeamsAsync();
            var usersTask = GetUsersAsync();

            await Task.WhenAll(projectsTask, tasksTask, teamsTask, usersTask);

            projects = await projectsTask;
            tasks = await tasksTask;
            teams = await teamsTask;
            users = await usersTask; 
        }

        public List<Projects> MergeData()
        {
            //Join Users to teams 
            teams = teams.GroupJoin(users,
                t => t.Id,
                u => u.TeamId,
                (tm, usrs) => new Teams
                {
                    Id = tm.Id,
                    Name = tm.Name,
                    CreatedAt = tm.CreatedAt,
                    Users = usrs
                }).ToList();

            //Join Performers to tasks
            tasks = tasks.Join(users,
                t => t.PerformerId,
                u => u.Id,
                (t, u) => new Tasks
                {
                    Id = t.Id,
                    ProjectId = t.ProjectId,
                    PerformerId = t.PerformerId,
                    Name = t.Name,
                    Description = t.Description,
                    State = t.State,
                    CreatedAt = t.CreatedAt,
                    FinishedAt = t.FinishedAt,
                    Performer = u
                }).ToList();

            //Join Tasks to Projects also add team and author
            projects = projects.GroupJoin(tasks,
                p => p.Id,
                t => t.ProjectId,
                (prj, tasks) => new Projects
                {
                    Id = prj.Id,
                    AuthorId = prj.AuthorId,
                    TeamId = prj.TeamId,
                    Name = prj.Name,
                    Description = prj.Description,
                    Deadline = prj.Deadline,
                    CreatedAt = prj.CreatedAt,
                    Tasks = tasks,
                    Team = teams.FirstOrDefault(x => x.Id == prj.TeamId),
                    Author = users.FirstOrDefault(x => x.Id == prj.AuthorId)
                }).ToList();
            return projects;
        }
        private async Task<string> LoadAsync(string url)
        {
            var webRequest = new HttpRequestMessage(HttpMethod.Get, url);
            var response = await client.SendAsync(webRequest);
            using var reader = new StreamReader(response.Content.ReadAsStream());
            if (response.IsSuccessStatusCode)
                return reader.ReadToEnd();
            else
                throw new Exception("Get request is unsuccessful.");
        }

        public async Task<int> MarkRandomTaskWithDelay(int tics)
        {
            await Task.Delay(tics); 
            Random rnd = new Random();
            var tasksList = await GetTasksAsync();
            var id = rnd.Next(1, tasksList.Count);

            var tsk = tasksList.Where(x=>x.Id == id).FirstOrDefault();
            tsk.FinishedAt = DateTime.Now;
            string request = JsonConvert.SerializeObject(tsk);
            var httpResponce = await client.PostAsync("api/projects", new StringContent(request, Encoding.UTF8, "application/json"));
            if (!httpResponce.IsSuccessStatusCode)
                throw new Exception("Request is failed");
            return id; 
        }

        private async Task<List<Projects>> GetProjectsAsync()
        {
            var result = await LoadAsync(ApiUrl + "projects");
            return JsonConvert.DeserializeObject<List<Projects>>(result);
        }
        private async Task<Projects> GetProjectByIdAsync(int id)
        {
            var result = await LoadAsync(ApiUrl + $"projects/{id}");
            return JsonConvert.DeserializeObject<Projects>(result);
        }

        private async Task<List<Users>> GetUsersAsync()
        {
            var result = await LoadAsync(ApiUrl + "users");
            return JsonConvert.DeserializeObject<List<Users>>(result);
        }
        private async Task<Users> GetUserByIdAsync(int id)
        {
            var result = await LoadAsync(ApiUrl + $"users/{id}");
            return JsonConvert.DeserializeObject<Users>(result);
        }

        private async Task<List<Tasks>> GetTasksAsync()
        {
            var result = await LoadAsync(ApiUrl + "tasks");
            return JsonConvert.DeserializeObject<List<Tasks>>(result);
        }
        private async Task<Tasks> GetTaskByIdAsync(int id)
        {
            var result = await LoadAsync(ApiUrl + $"tasks/{id}");
            return JsonConvert.DeserializeObject<Tasks>(result);
        }
        private async Task<List<Teams>> GetTeamsAsync()
        {
            var result = await LoadAsync(ApiUrl + "teams");
            return JsonConvert.DeserializeObject<List<Teams>>(result);
        }
        private async Task<Teams> GetTeamsByIdAsync(int id)
        {
            var result = await LoadAsync (ApiUrl + $"teams/{id}");
            return JsonConvert.DeserializeObject<Teams>(result);
        }
    }
}
