﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Text; 

namespace ConsoleClient.Services
{
    public class ConfigurationService
    {
        public static string ReadApiUrl()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                                                .SetBasePath(Directory.GetCurrentDirectory())
                                                .AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            return config.GetSection("AppSettings")["ApiUrl"];
        }
    }
}
