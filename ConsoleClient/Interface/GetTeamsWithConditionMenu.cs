﻿using System;

namespace ConsoleClient.Models.Interface
{
    public class GetTeamsWithConditionMenu : IState
    {
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Отримати список з колекції команд, учасники яких старші 10 років (Введіть quit для виходу): ");
            var result = Program.service.GetTeamsWithSortedUserOlderTen();

            Console.WriteLine("   Task Id    Users count     Task Name ");

            foreach (var item in result)
            {
                Console.WriteLine($"      {item.Id}         {item.Users.Count}         {item.Name} ");
            }

            ColorConsole.GreenColorOutput("Натисніть Enter щоб повернутись до головного меню");
            Console.ReadLine();
            Console.Clear();
            return new MainMenu();
        }
    }
}
