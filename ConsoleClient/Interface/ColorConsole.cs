﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Models.Interface
{
    public static class ColorConsole
    {
        /// <summary>
        /// Вивести повідомлення до консолі зеленого кольору
        /// </summary>
        /// <param name="input">Текст, що необхідно відобразити в консолі</param>
        public static void GreenColorOutput(string input)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(input);
            Console.ResetColor();
        }
        /// <summary>
        /// Вивести повідомлення до консолі червоного кольору
        /// </summary>
        /// <param name="input">Текст, що необхідно відобразити в консолі</param>
        public static void RedColorOutput(string input)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(input);
            Console.ResetColor();
        }


    }
}
