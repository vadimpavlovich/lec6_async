﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Models.Interface
{
    /// <summary>
    /// Клас пункту меню
    /// </summary>
    public class MenuItem
    {
        public string Text { get; set; }
    }
    /// <summary>
    /// Клас для управління станом меню та навігації
    /// </summary>
    public abstract class MenuState : IState
    {
        protected abstract Dictionary<int, MenuItem> Menus { get; }

        protected virtual void ShowMenu()
        {
            foreach (var m in Menus)
                Console.WriteLine($"{m.Key} - {m.Value.Text}");
        }

        protected virtual KeyValuePair<int, MenuItem> ReadOption()
        {
            Console.WriteLine("Будь-ласка оберіть пункт меню:");
            ShowMenu();

            var str = Console.ReadLine();
            int answerId = 0;

            if (int.TryParse(str, out answerId))
            {
                if (!Menus.ContainsKey(answerId))
                {
                    Console.Clear();
                    Console.WriteLine($"Пункту {str} не існує.");
                    return ReadOption();
                }
                return new KeyValuePair<int, MenuItem>(answerId, Menus[answerId]);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Ведено невірні дані.");
                return ReadOption();
            }
        }

        public virtual IState RunState()
        {
            var option = ReadOption();
            return NextState(option);
        }

        protected abstract IState NextState(KeyValuePair<int, MenuItem> selectedMenu);
    }
}
