﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Models.Interface
{
    public static class MenuHelpers
    {
        public static bool isQuit(string input)
        {
            if (input == "quit")
            {
                Console.Clear();
                return true;
            }
            return false;
        }

        public static int? ReadCorrectUserIdFromInput()
        {
            bool valueValid = false;
            int userId = 0;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Введіть коректний Id користувача: ");
                var input = Console.ReadLine();
                if (MenuHelpers.isQuit(input))
                    return null;

                var correct = Int32.TryParse(input, out userId);
                if (correct)
                {
                    if (Program.service.api.users.Where(x => x.Id == userId).FirstOrDefault() != null)
                        valueValid = true;
                    else
                        ColorConsole.RedColorOutput($"Користувач з Id {input} не існує. ");
                }
                else
                {
                    ColorConsole.RedColorOutput($"Введене значення = {input} - не є числом. ");
                }
            }
            return userId;
        }
    }
}
